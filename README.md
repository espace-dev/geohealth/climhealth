## ClimHealth <img src="www/img/hex-climhealth.png" align="right" height="139"/>

[![Project Status: Inactive – The project has reached a stable, usable
state but is no longer being actively developed; support/maintenance
will be provided as time
allows.](https://www.repostatus.org/badges/latest/inactive.svg)](https://www.repostatus.org/#inactive)

The [ClimHealth](https://www.spaceclimateobservatory.org/climhealth-yangon) project aims to integrate climate and environmental information from satellites into health surveillance systems to develop early warning and guide disease control. This tool allows to visualize epidemiological information together with climate/environment information within a dashboard. It has been developed in R language, using R-Shiny.

``` r
renv::activate() # Activate the project's local R environment
renv::restore() # Restore the project's R library to a known state
renv::status() # Check the status of the environment
shiny::runApp() # Run the app
```

## Contributing
This project is realized by [IRD](https://en.ird.fr/) [UMR Espace-Dev](https://www.espace-dev.fr/) in partnership with [Université de La Réunion](https://www.univ-reunion.fr/), [SEAS-OI](http://www.seas-oi.org/), [Mahidol University](https://www.tm.mahidol.ac.th/social-environment/chias/) in Thailand and [Institut Pasteur du Cambodge](https://www.pasteur-kh.org/):
- Vincent Herbreteau (conceptualization & coordination)
- Lucas Longour (conceptualization and development)
- Florian Girond (conceptualization)


## Funding
ClimHealth was funded by [CNES](https://cnes.fr/en) and has been accredited by the [Space Climate Observatory (SCO) International Initiative](https://www.spaceclimateobservatory.org/).
