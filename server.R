server <- function(input, output, session) {
  timestamp_to_datetime <- function(x){
    x %>%
      as.numeric() %>%
      `/`(1000) %>%
      as.POSIXct(origin = "1970-01-01") %>%
      as.Date(format = "%Y-%m-%d")
  }
  
  output$plot <- renderHighchart({
    highchart(type = "stock") %>%
      hc_yAxis_multiples(
        list(title=list(text="<b>Cases<b>", margin=20),
             opposite = FALSE,
             height = "50%",
             min=0),
        list(title=list(text="<b>Precipitation (mm) <b>", margin=20),
             showLastLabel = TRUE,
             offset=0,
             top = "50%",
             height = "50%",
             opposite = FALSE,
             reversed = TRUE, min=0),
        list(title=list(text="<b>Temperature (°C) <b>", margin=20),
             showLastLabel = TRUE,
             offset=50,
             height = "50%",
             opposite = TRUE,
             min=0)
      ) %>%
      hc_add_series(
        arrange(filter(diseases, disease %in% input$select_disease & province %in% input$select_province), ymd(date)),
        if (input$select_variable == "cases") {
          hcaes(x = date, y = cases)
        } else {
          hcaes(x = date, y = deaths)
        },
        type = "line",
        lineWidth = 1,
        color="#FF6633",
        name= paste(input$select_disease),
        yAxis = 0
      ) %>%
      hc_add_series(arrange(filter(env, indice %in% "Precipitation" & province %in% input$select_province), ymd(date)),
                    hcaes(x = date, y = value),
                    type = "column",
                    color = "#03aaf9",
                    name = paste("Precipitation"),
                    yAxis = 1
      ) %>%
      hc_add_series(arrange(filter(env, indice %in% "Temperature" & province %in% input$select_province), ymd(date)),
                    hcaes(x = date, y = value),
                    type = "spline",
                    height = "50%",
                    lineWidth =2,
                    dashStyle = "longdash",
                    color = "#fab666",
                    name = paste("Temperature"),
                    yAxis = 2
      ) %>%
      hc_legend(enabled = TRUE) %>%
      hc_add_event_point(event = "click") %>%
      hc_xAxis(type = "datetime") %>%
      hc_rangeSelector( buttons = list(
        list(type = 'all', text = 'All'),
        list(type = 'year', count = 10, text = '10 y.'),
        list(type = 'year', count = 5, text = '5 y.'),
        list(type = 'year', count = 1, text = '1 y.')
      ),
      selected = 2,
      verticalAlign = "bottom") 
  })
  
  output$map <- renderLeaflet({
    leaflet() %>%
      fitBounds(97.3438, 5.6128, 105.5368, 20.4648) %>%
      addProviderTiles(providers$CartoDB.PositronNoLabels) %>%
      addPolygons(data = province,
                  layerId = ~adm1_pcode,
                  weight = 1) %>%
      addScaleBar(
        position = "bottomleft",
        options = scaleBarOptions(maxWidth = 100, metric = TRUE, imperial = FALSE, updateWhenIdle = TRUE)) 
  })
  
  map_proxy = leafletProxy("map")
  
  observe({
    click <- input$map_shape_click
    
    select_date = if (is.null(input$plot_click)){max(diseases$date)} else {timestamp_to_datetime(input$plot_click$x)}
    highchartProxy("plot") %>%
      hcpxy_update(
        xAxis = list(
          plotLines = list(
            list(
              color = "#FF0000",
              width = 1,
              value = input$plot_click$x
            )
          )
        )
      )
    
    selection = filter(diseases, disease %in% input$select_disease & province != "National" & date == select_date)
    selection_data <- diseases[diseases$disease == input$select_disease & diseases$date == select_date,]
    
    pal_cases <- colorBin("Blues", selection_data$cases_incidence, bins = 7, na.color = NA)
    pal_deaths <- colorBin("Greens", selection_data$deaths_incidence, bins = 7, na.color = NA)
    map_proxy %>%
      clearMarkers() %>%
      clearControls() %>%
      setShapeStyle(
        data = selection_data,
        layerId = ~adm1_pcode,
        fillColor = if (input$select_variable == "cases"){~pal_cases(cases_incidence)}else{~pal_deaths(deaths_incidence)},
        fillOpacity = .8,
        color = "black",
        opacity = .7
      ) %>%
      setShapeLabel(
        data = selection_data,
        layerId = ~adm1_pcode,
        label = ~lapply(paste(
          "<i>", province, "</i><br>",
          "<b>Cases: </b>", cases, "<br>",
          "<b>Cases incidence (per 100k): </b>", cases_incidence, "<br>",
          "<b>Deaths: </b>", deaths, "<br>",
          "<b>Deaths incidence (per 100k): </b>", deaths_incidence, "<br>"
        ), HTML)
      ) %>%
      addLegend(pal = if (input$select_variable == "cases"){pal_cases}else{pal_deaths}, values = if (input$select_variable == "cases"){selection_data$cases_incidence}else{selection_data$deaths_incidence}, opacity = 0.7, title = if (input$select_variable == "cases"){"Cases incidence (per 100k)"}else{"Deaths incidence (per 100k)"},
                position = "bottomright") %>%
      addControl(tags$div(HTML(paste0("<h4>", select_date, "</h4>"))) , position = "bottomleft")
    
    updatePickerInput(session = session, inputId = "select_province",
                      selected = if (is.null(click)){"National"}else{province_pcode[province_pcode$adm1_pcode == click$id,]$province})
    
  })
  
}