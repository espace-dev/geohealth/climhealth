var header = $('.navbar > .container-fluid');
var imgContainer = $('<div style="display:flex; align-items:center;"></div>');

var moph = $('<a href="https://moph.go.th/" target="_blank"><img src="img/MoPH.png" alt="alt" style="float:left;height:30px;margin-right:10px;"></a>');
imgContainer.append(moph);

var ird = $('<a href="https://en.ird.fr/" target="_blank"><img src="img/logo_IRD_2016_BLOC_UK_BLANC.png" alt="alt" style="float:left;height:41px;margin-right:10px;"></a>');
imgContainer.append(ird);

var cnes = $('<a href="https://cnes.fr/en" target="_blank"><img src="img/CNES.png" alt="alt" style="float:left;height:41px;margin-right:10px;"></a>');
imgContainer.append(cnes);

header.append(imgContainer);

console.log(header);